BASENAME=hamradio-handbook
SOURCE=$(BASENAME).xml

.PHONY: all tex html pdf validate publish clean

all: html pdf tex

tex: $(BASENAME).tex
$(BASENAME).tex: $(SOURCE)
	docbook2tex $<

html: $(BASENAME).html
$(BASENAME).html: $(SOURCE)
	docbook2html $<

pdf: $(BASENAME).pdf
$(BASENAME).pdf: $(SOURCE)
	docbook2pdf $<

validate:
	xmllint --valid --noout $(SOURCE)

# publishing to the DDP web pages
publish: all
	test -d $(PUBLISHDIR)/$(BASENAME) || $(install_dir) $(PUBLISHDIR)/$(BASENAME)
	rm -f $(PUBLISHDIR)/$(BASENAME)/*
	$(install_file) *.html $(PUBLISHDIR)/$(BASENAME)/
	$(install_file) *.pdf $(PUBLISHDIR)/$(BASENAME)/
	$(install_file) *.tex $(PUBLISHDIR)/$(BASENAME)/

clean:
	rm -f *html *pdf *ps *dvi *tex
